import express from 'express'
import authController from '../controllers/auth'
import mapsController from '../controllers/maps'

export default function (app) {
    app.use('/login', authController)
    app.use('/maps', mapsController)
}