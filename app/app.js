import express from 'express'
import bodyParser from 'body-parser'
import routes from './routes'
import cors from 'cors'
import jwt from 'jsonwebtoken'

require('dotenv').config()

var app = express()

app.use(cors())
app.use(bodyParser.json({limit:'50mb'}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))

routes(app)

app.listen(process.env.APP_PORT, function () {
    console.log('Server is running in port ' + process.env.APP_PORT)
})