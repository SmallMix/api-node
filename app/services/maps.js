export default {
    getAll (req, res, next) {
        var data = [
            {
                lat: -25.470991, 
                lon: -49.271036
            },
            {
                lat: -0.935586,
                lon: -49.635540
            },
            {
                lat: -2.485874, 
                lon: -43.128493
            }
        ]
    
        res.send(JSON.stringify(data))
    }
}