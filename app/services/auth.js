import jwt from 'jsonwebtoken'

export default {
    login (req, res, next) {
        require('dotenv').config()
        var body = req.body
        if (body.username == 'michel' && body.password == '123') {
            var token = jwt.sign({name: 'michel'}, process.env.JWT_SECRET, {expiresIn: 60000})
            res.send(JSON.stringify({auth: true, token: token}))
        } else {
            res.status(401).send({auth: false, message: 'Invalid credentials!'})
        }
    }
}