import express from 'express'
import mapsService from '../services/maps'
var routes = require('../routes')

const router = express.Router()

router.get('/', mapsService.getAll)

export default router