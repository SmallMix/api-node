import express from 'express'
import authService from '../services/auth'

const router = express.Router()

router.post('/', authService.login)

export default router